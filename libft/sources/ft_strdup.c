/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 16:09:39 by abary             #+#    #+#             */
/*   Updated: 2015/12/24 13:20:23 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char			*result;
	unsigned int	nb;

	nb = ft_strlen(s1);
	result = (char *)ft_memalloc(sizeof(char) * nb + 1);
	if (!result)
		return (NULL);
	nb = 0;
	while (*s1)
	{
		*(result + nb) = *s1;
		s1++;
		nb++;
	}
	return (result);
}
